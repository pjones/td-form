const gulp = require("gulp"),
  connect = require("gulp-connect"),
  minify = require("gulp-minifier"),
  sass = require("gulp-sass"),
  cssnano = require("gulp-cssnano"),
  sourcemaps = require("gulp-sourcemaps"),
  imagemin = require("gulp-imagemin"),
  util = require("gulp-util");

gulp.task("connect", function() {
  connect.server({
    root: "./prod",
    port: 8082,
    livereload: true
  });
});

gulp.task("html", () =>
  gulp
    .src("./dev/*.html")
    .pipe(
      minify({
        minify: true,
        minifyHTML: {
          collapseWhitespace: true,
          conservativeCollapse: true
        }
      })
    )
    .pipe(gulp.dest("./prod"))
    .pipe(connect.reload())
);

gulp.task("sass", () =>
  gulp
    .src("./dev/sass/**/*.scss")
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest("./dev/css"))
    .pipe(cssnano())
    .pipe(sourcemaps.write("../maps"))
    .pipe(gulp.dest("./prod/css"))
    .pipe(connect.reload())
);

gulp.task("js", () =>
  gulp
    .src("./dev/js/*.js")
    .pipe(
      minify({
        minify: true,
        minifyJS: {
          sourceMap: true
        }
      })
    )
    .pipe(gulp.dest("./prod/js"))
    .pipe(connect.reload())
);

gulp.task("img", () =>
  gulp
    .src("./dev/images/*.*")
    .pipe(imagemin())
    .pipe(gulp.dest("./prod/images"))
);

gulp.task("fonts", () =>
  gulp
    .src("./dev/fonts/*.*")
    .pipe(gulp.dest("./prod/fonts"))
    .on("error", util.log)
);

gulp.task("watch", () =>
  gulp.watch(
    ["./dev/*.html", "./dev/sass/**/*.*", "./dev/js/*.*"],
    ["html", "sass", "js"]
  )
);

/* build (or update) the prod folder */
gulp.task("build", ["html", "sass", "js", "img", "fonts"]);

/* start the server and get to work */
gulp.task("default", ["connect", "watch"]);
/* Peter A. Jones */
