# Instructions

1. If you don't have nodejs installed, you can get it [here](https://nodejs.org/en/).
2. Download or clone this repository i.e. `git clone https://gitlab.com/pjones/td-form.git`
3. From the repository directory, run the command `npm install`. This will install the dependencies I used in development.
4. After the dependencies have been downloaded, run the command `gulp build`. This will create the production folder 'prod'. What is happening is the HTML and JS are being copied from 'dev' to 'prod' and minified, the SASS files are being transpiled to CSS and copied to 'prod' and the images are being minified before being copied to 'prod'.
5. Now that the production files are ready, run the command `gulp` to start the web server. You can now point your browser to `http://localhost:8082`

NOTE: When developing like this, the browser will reload when any of the SASS/CSS or HTML files have been saved on change.

### Alternative

Just open the `index.html` file from the 'prod' directory in your browser. This is why I left the already compiled 'prod' directory in the repo.
