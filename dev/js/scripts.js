// Javascript
const hamburger = document.getElementById("hamburger");

hamburger.addEventListener("click", ele => {
  ele.preventDefault();
  const menu = document.getElementById("slidemenu");
  if (menu.style.width === "0px") {
    document.getElementById("slidemenu").style.width = "60vW";
    document.getElementById("slidemenu").style.opacity = "1";
  } else {
    document.getElementById("slidemenu").style.width = "0";
    document.getElementById("slidemenu").style.opacity = "0";
  }
});
